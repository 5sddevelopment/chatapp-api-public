const admin = require('firebase-admin');
const serviceAccount = require("../config/firebaseConfig");

const app = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

module.exports = {
  sendNotification: (deviceToken, notifyTitle, notifyBody, myData = JSON.stringify({})) => {
    if (!deviceToken) {
      return false
    }
    if (!Array.isArray(deviceToken)) {
      deviceToken = [deviceToken]
    }
    const message = {
      data: { data: myData },
      notification: {
        title: notifyTitle,
        body: notifyBody
      },
      tokens: deviceToken
    };
    console.log(deviceToken);
    if (deviceToken.length >= 1) {
      const messaging = app.messaging();
      messaging.sendMulticast(message)
        .then((response) => {
          // Response is a message ID string.
          console.log('Successfully sent message:', JSON.stringify(response));
        })
        .catch((error) => {
          console.log('Error sending message:', error);
        });
    }
  }
}
