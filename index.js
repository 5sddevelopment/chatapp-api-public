const express = require("express");
const app = express();
const { PORT, NODE_ENV, PRIV_KEY_PATH, CERT_PATH } = require("./config/globalConfig");
const cors = require('cors');
const fs = require('fs');
const https = require("https");

app.use(cors())
app.use(express.static('public'));
app.use("/public", express.static('public'));

app.use(express.urlencoded({
  extended: false
}));
app.use(express.json());

require("./startup/routes")(app);

let server;
//For Dev http
if (NODE_ENV == "development") {
  server = app.listen(PORT, () => console.log(`Listening on port: ${PORT}, and the environment is: ${NODE_ENV}`));
}
else {
  const options = {
    key: fs.readFileSync(PRIV_KEY_PATH),
    cert: fs.readFileSync(CERT_PATH),
    passphrase: ''
  };
  server = https.createServer(options, app).listen(PORT, () => console.log(`Listening on port: ${PORT}, and the environment is: ${NODE_ENV}`));
}

// connecting socket
require("./chat")(server);
