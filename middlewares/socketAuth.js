const jwtUtils = require("../utils/jwt")

module.exports.socketAuth = (socket, next) => {
  try {
    const { token } = socket.handshake.auth
    if (!token) {
      const err = new Error("not authorized");
      err.data = { content: "Missing Auth Token" }; // additional details
      next(err);
    } else {
      // verifing auth token
      const decoded = jwtUtils.verifyJWT(token)
      socket.user = decoded
      next()
    }
  } catch (error) {
    console.log(error)
    const err = new Error(error.message);
    next(err)
  }
}