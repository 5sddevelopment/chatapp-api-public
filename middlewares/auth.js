const Response = require("../utils/response");
const jwtUtils = require("../utils/jwt")

module.exports = function (req, res, next) {
  const token = req.header('x-auth-token')
  if (!token) return res.status(401).send(Response.failure(401, "Access denied no token provided"))
  try {
    const decoded = jwtUtils.verifyJWT(token)
    req.user = decoded
    next()
  } catch (error) {
    console.log(error)
    res.status(401).send(Response.failure(401, "Invalid code"))
  }
}