const Response = require("../utils/response");
const Model = require("../models");
const bcrypt = require("bcryptjs")
const jwtUtils = require("../utils/jwt")

module.exports = {
  signup: async function (req, res) {
    try {
      const { email, password, fullName, deviceToken } = req.body
      const userExist = await Model.User.findOne({ where: { email } })
      if (userExist) {
        return res.status(400).send(Response.failure(400, "Account already exist with this email."));
      }
      const salt = await bcrypt.genSalt(10)
      const hashedPassword = await bcrypt.hash(password, salt)
      // const hashedPassword = await bcrypt.hash(password, bcrypt.genSalt(10));
      let userCreated = await Model.User.create({ email, password: hashedPassword, fullName, deviceToken, status: true })
      userCreated = JSON.parse(JSON.stringify(userCreated))
      delete userCreated.password
      // generate auth token
      const authToken = jwtUtils.generateJWT(userCreated)
      userCreated['authToken'] = authToken
      return res.status(201).send(Response.success(201, { user: userCreated, message: "Account created successfully" }));
    } catch (error) {
      console.error(error)
      return res.status(500).send(Response.failure(500, "Server Error"))
    }
  },
  login: async function (req, res) {
    try {
      const { email, password, fcmToken } = req.body
      let userExist = await Model.User.findOne({ where: { email } })
      if (!userExist) {
        return res.status(400).send(Response.failure(400, "Incorrect email or password"));
      }
      const isCorrectPassword = await bcrypt.compare(password, userExist.password)
      if (!isCorrectPassword) {
        return res.status(400).send(Response.failure(400, "Incorrect email or password"));
      }
      userExist = JSON.parse(JSON.stringify(userExist))
      delete userExist.password

      const authToken = jwtUtils.generateJWT(userExist)
      userExist['authToken'] = authToken
      if (fcmToken) {
        await Model.User.update({ fcmToken }, { where: { id: userExist.id } })
      }
      return res.status(200).send(Response.success(200, { user: userExist, message: "Login successfully" }));
    } catch (error) {
      console.error(error)
      return res.status(500).send(Response.failure(500, "Server Error"))
    }
  },
  logout: async function (req, res) {
    try {
      await Model.User.update({ fcmToken: null, socketId: null }, { where: { id: req.user.id } })
      return res.status(200).send(Response.success(200, { user: {}, message: "Logout successfully" }));
    } catch (error) {
      console.error(error)
      return res.status(500).send(Response.failure(500, "Server Error"))
    }
  },
}


