const Response = require("../utils/response");
const Model = require("../models");
const s3Utils = require("../utils/s3")
const { deleteFiles } = require("../utils/deleteFile")
const generateThumbnail = require("../utils/generateThumbnail")

module.exports = {
  getChatOfChannel: async (req, res) => {
    try {
      const chat = await Model.Chat.findAll({
        limit: Number(req.query.limit) || 10,
        offset: Number(req.query.offset) || 0,
        attributes: ["id", "senderId", "messageReplyId", "messageType", "message", "createdAt", "status"],
        where: {
          channelId: req.params.id
        },
        order: [
          ['createdAt', 'DESC']
        ],
      })
      return res.status(200).send(Response.success(200, { message: "Data Fetched Successfully", chat }))
    } catch (error) {
      console.error(error)
      return res.status(500).send(Response.failure(500, "Server Error"))
    }
  },
  uploadMedia: async (req, res) => {
    const { files } = req
    if (!files) {
      return res.status(400).send(Response.failure(400, "Media is required"));
    }

    try {
      const uploadResponse = await s3Utils.uploadFilesToS3(files, req.user.id)
      // creating video thumbnail
      const thumbnailFolder = `./public/uploads/${req.user.id}/thumbnails`;
      for (let i = 0; i < files.length; i++) {
        if (files[i].mimetype.split("/")[0] == "video") {
          const thumbnailName = files[i].fieldname + "_" + "thumbnail.png"
          // generating thumbnail
          await generateThumbnail(files[i].path, thumbnailName, thumbnailFolder)
          const thumbnailPath = thumbnailFolder + "/" + thumbnailName
          // uploading thumbnail to s3
          const thumbnailUploadResponse = await s3Utils.uploadSingleFileToS3(thumbnailName, thumbnailPath, req.user.id)
          files[i].thumbnail = thumbnailUploadResponse.Location
          // deleting thumbnail file
          await deleteFiles([{ path: thumbnailPath }])
        }
      }
      // delete files after uploading to s3
      await deleteFiles(files)
      for (let i = 0; i < files.length; i++) {
        files[i]['path'] = uploadResponse[i].Location
      }
      const { timestamp } = req.body
      return res.status(200).send(Response.success(200, { message: "Media Uploaded Successfully", timestamp, media: files }))
    } catch (error) {
      console.error(error)
      return res.status(500).send(Response.failure(500, "Server Error"))
    }
  },
}


