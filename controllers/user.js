const Response = require("../utils/response");
const Model = require("../models");
const bcrypt = require("bcryptjs")
const jwtUtils = require("../utils/jwt")
const { Op } = require('sequelize')

module.exports = {
  getAllUsers: async function (req, res) {
    try {
      const users = await Model.User.findAndCountAll({
        attributes: ["id", "fullName", "email", "createdAt", "socketId"],
        where: { id: { [Op.ne]: req.user.id } }
      })
      return res.status(200).send(Response.success(200, { users, message: "Account created successfully" }));
    } catch (error) {
      console.error(error)
      return res.status(500).send(Response.failure(500, "Server Error"))
    }
  }
}


