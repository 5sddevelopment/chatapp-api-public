'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ChatChannel extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ChatChannel.hasMany(models.ChatMember, {
        foreignKey: 'channelId'
      })

      ChatChannel.hasMany(models.Chat, {
        foreignKey: 'channelId'
      })
    }
  }
  ChatChannel.init({
    title: DataTypes.STRING,
    startFromUserId: DataTypes.INTEGER,
    lastMessage: DataTypes.TEXT,
    lastMessageType: DataTypes.STRING,
    type: DataTypes.ENUM("oneToOne", "group"),
    status: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    sequelize,
    modelName: 'ChatChannel',
  });
  return ChatChannel;
};