'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Chat extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Chat.init({
    channelId: DataTypes.INTEGER,
    senderId: DataTypes.INTEGER,
    messageReplyId: DataTypes.INTEGER,
    message: DataTypes.TEXT,
    messageType: {
      type: DataTypes.STRING,
      defaultValue: 'text'
    },
    status: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    sequelize,
    modelName: 'Chat',
  });
  return Chat;
};