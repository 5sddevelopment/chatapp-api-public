const authRoute = require("../routes/v1/auth");
const usersRoute = require("../routes/v1/user");
const chatRoute = require("../routes/v1/chat");

module.exports = function (app) {
  app.use(`/${process.env.URL_STAGE || "dev"}/chat-app/api/v1/auth`, authRoute);
  app.use(`/${process.env.URL_STAGE || "dev"}/chat-app/api/v1/user`, usersRoute);
  app.use(`/${process.env.URL_STAGE || "dev"}/chat-app/api/v1/chat`, chatRoute);
};