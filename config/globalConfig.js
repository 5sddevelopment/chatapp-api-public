
const dotenv = require('dotenv');
dotenv.config();
const { PORT, JWT_PRIVATE_KEY, NODE_ENV, PRIV_KEY_PATH, CERT_PATH, DB_USERNAME, DB_PASSWORD, DB_NAME, DB_HOST } = process.env

module.exports = {
  PORT: PORT || 3000,
  JWT_PRIVATE_KEY,
  NODE_ENV,
  PRIV_KEY_PATH,
  CERT_PATH,
  DB_USERNAME,
  DB_PASSWORD,
  DB_NAME,
  DB_HOST
}
