const express = require("express");
const router = express.Router();
const chatController = require('../../controllers/chat');
const auth = require("../../middlewares/auth")
const { uploadMedia } = require('../../services/fileUpload')


router.get("/:id", auth, chatController.getChatOfChannel);

router.post("/upload-media", auth, uploadMedia.array('media', 5), chatController.uploadMedia);

module.exports = router;