const express = require("express");
const router = express.Router();
const authController = require('../../controllers/auth');
const auth = require("../../middlewares/auth")

router.post("/signup", authController.signup);

router.post("/login", authController.login);

router.post("/logout", auth, authController.logout);

module.exports = router;