const sharp = require('sharp')
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path
const ffmpeg = require('fluent-ffmpeg')
ffmpeg.setFfmpegPath(ffmpegPath)


module.exports = {
  compressImage: async (sourcePath, targetPath, extension) => {
    let Gifsicle = await import('gifsicle-wrapper');
    Gifsicle = Gifsicle.default
    try {
      switch (extension) {
        case '.gif':
          await Gifsicle(sourcePath)
            .optimize({ level: Gifsicle.level.O3, lossiness: 30 })
            .toFile(targetPath);
          break
        case '.png':
          await sharp(sourcePath).png({ quality: 30 }).toFile(targetPath)
          break
        default:
          await sharp(sourcePath).jpeg({ quality: 30 }).toFile(targetPath)
          break
      }
    } catch (error) {
      console.log(error);
      console.log('Error in compressing Image: ', error.message)
    }
  },
  compressVideo: async (sourcePath, targetPath, fps = 50) => {
    return new Promise(function (resolve, reject) {
      ffmpeg({ source: sourcePath })
        .fps(fps)
        .addOptions(["-crf 28"])
        .on("end", async () => {
          resolve("Done!")
        })
        .on("error", (err) => {
          reject(err)
          console.log("error in compressing video");
        })
        .save(targetPath)
    });
  }
}