const fs = require("fs")

module.exports.deleteFiles = (filesArray) => {
  try {
    if (!Array.isArray(filesArray)) {
      console.log("File Array is required to delete files.")
    }
    for (const file of filesArray) {
      fs.unlink(file.path, (err) => { if (err) console.log(err) })
    }
  } catch (error) {
    console.log(error);
  }
}