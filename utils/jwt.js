const jwt = require("jsonwebtoken")
const { JWT_PRIVATE_KEY } = require("../config/globalConfig");

module.exports = {
  generateJWT: (payload) => {
    return jwt.sign(payload, JWT_PRIVATE_KEY);
  },
  verifyJWT: (token) => {
    return jwt.verify(token, JWT_PRIVATE_KEY);
  },
}
