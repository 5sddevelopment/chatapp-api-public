const AWS = require('aws-sdk');
const fs = require('fs');

const s3 = new AWS.S3({
  accessKeyId: process.env.S3_BUCKET_ACCESS_KEY,
  secretAccessKey: process.env.S3_BUCKET_SECRET_KEY,
  region: process.env.S3_BUCKET_REGION
});

module.exports = {
  uploadFilesToS3: (filesArray, userId) => {
    if (!Array.isArray(filesArray)) {
      console.log("File Array is required in upload s3 function.")
    }
    try {
      const fileUplaodingPromises = []
      for (const file of filesArray) {
        const params = {
          Bucket: process.env.S3_BUCKET_NAME,
          Key: `${userId}/${Date.now()}_${file.originalname}`,
          Body: fs.createReadStream(file.path),
        };
        fileUplaodingPromises.push(s3.upload(params).promise())
      }
      return Promise.all(fileUplaodingPromises)
    } catch (error) {
      console.log(error);
    }
  },
  uploadSingleFileToS3: (fileName, filePath, userId) => {
    try {
      const params = {
        Bucket: process.env.S3_BUCKET_NAME,
        Key: `${userId}/${Date.now()}_${fileName}`,
        Body: fs.createReadStream(filePath),
      };
      return s3.upload(params).promise()
    } catch (error) {
      console.log(error);
    }
  }
}

