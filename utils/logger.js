module.exports = {
  printEventWithData: (event, data) => {
    console.log(`event: ${event} with data => ${JSON.stringify(data)}`)
  },
  printErrorLog: (event, error) => {
    console.log(`Error in event: ${event} => ${error}`)
  }
}