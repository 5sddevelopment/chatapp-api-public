const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);

module.exports = async (filePath, thumbnailName, thumbnailFolder) => {
  return new Promise((resolve, reject) => {
    ffmpeg(filePath)
      .screenshots({
        timemarks: ['2'], // seconds
        count: 1,
        filename: thumbnailName,
        folder: thumbnailFolder,
        size: '500x750'
      })
      .on("end", async () => {
        resolve("Done!")
      })
      .on("error", (err) => {
        reject(err)
        console.log("error in generating thumbnail");
      })
  })
}