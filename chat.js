const Model = require("./models")
const { socketAuth } = require("./middlewares/socketAuth")
const { sequelize } = require("./models");
const { Op, QueryTypes } = require("sequelize")
const notificationService = require("./services/notification")
const helpers = require("./helpers")
const onlineUsers = {}
const { printEventWithData, printErrorLog } = require("./utils/logger")
const { MESSAGE_STATUS } = require("./config/constant")

module.exports = (server) => {
  const io = require("socket.io")(server, {
    cors: {
      origin: "*",
      methods: ["GET", "POST"]
    }
  });

  // auth middleware
  io.use(socketAuth);

  // connection
  io.on("connection", async socket => { // socket connection
    console.log("*********** User payload ", socket.user);
    const { user } = socket
    if (user) {
      onlineUsers[user.id] = true
      // emit to all users that a new user is online
      socket.broadcast.emit('check:online', onlineUsers)
      await helpers.updateUser({ socketId: socket.id }, user.id)
    }

    // message send and receive event
    socket.on("message:send", async (data, ackCallback) => { // { senderId: "", messageReplyId: "", receiverId: "", message: "", channelType: "", messageType: ""}
      printEventWithData('message:send', data)
      const { receiverIds } = data
      try {
        const response = []
        for (const receiverId of receiverIds) {
          const { senderId, message, messageReplyId, channelType, messageType } = data
          const messageResponse = await helpers.sendMessage(senderId, receiverId, message, channelType, messageType, messageReplyId, io, socket)
          data['channelId'] = messageResponse.channelId
          data['messageId'] = messageResponse.messageId
          response.push(data)
        }
        ackCallback({ response, event: 'message:send' })
      } catch (error) {
        printErrorLog('message:send', error.message)
      }
    })

    // message read for unread message count
    socket.on("message:read", async (data, ackCallback) => { // { channelId: "", otherUserId: ""}
      printEventWithData('message:read', data)
      try {
        const { channelId, otherUserId } = data
        await helpers.markMessagesAsRead(channelId, socket.user.id, otherUserId)
        const receiver = await Model.User.findOne({ where: { id: otherUserId } })
        if (receiver.socketId) {
          io.to(receiver.socketId).emit("message:read", { channelId });
        }
        // acknowledge to the sender
        ackCallback({ ...data, event: 'message:read' })
      } catch (error) {
        printErrorLog('message:read', error.message)
      }
    })

    // user online and offline status
    socket.on("check:online", async (data, ackCallback) => {
      printEventWithData('check:online', data)
      try {
        socket.emit("check:online", onlineUsers)
        // acknowledge to the sender
        ackCallback({ ...data, event: 'check:online' })
      } catch (error) {
        printErrorLog('check:online', error.message)
      }
    })

    // Chat channel get event
    socket.on("chathead:get", async (data, ackCallback) => {
      printEventWithData('chathead:get', data)
      try {
        const userId = socket.user.id
        const chatChannels = await helpers.getChatChannels(userId)
        socket.emit("chathead:get", chatChannels)
        // acknowledge to the sender
        ackCallback({ ...data, event: 'chathead:get' })
      } catch (error) {
        printErrorLog('chathead:get', error.message)
      }
    })

    // Make user online and offline 
    socket.on("online:offline", async (data, ackCallback) => {
      printEventWithData('online:offline', data)
      try {
        const { userId, onlineOrLastSeen } = data 
        // acknowledge to the sender
        onlineUsers[userId] = onlineOrLastSeen
        ackCallback({ ...data, event: 'online:offline' })
      } catch (error) {
        printErrorLog('online:offline', error.message)
      }
    })

    // disconnect event
    socket.on('disconnect', async () => {
      try {
        // set socket id of diconnecting user to null 
        const { user } = socket
        console.log("*********** socket disconnected => ", socket.id, " ** userId => ", user.id);
        onlineUsers[user.id] = new Date()
        // emit to all users that a user is offline
        socket.broadcast.emit('check:online', onlineUsers)
        await helpers.updateUser({ socketId: null }, user.id)
      } catch (error) {
        printErrorLog('disconnect', error.message)
      }
    });

  });
}

