const Model = require("../models")
const { Op, QueryTypes, Sequelize } = require("sequelize")
const { sequelize } = require("../models");
const notificationService = require("../services/notification")
const { MESSAGE_STATUS } = require("../config/constant")

const helperFunctions = {
  getChatChannels: async (userId) => {
    return Model.ChatChannel.findAll({
      where: {
        id: {
          [Op.in]: [Sequelize.literal(`SELECT channelId FROM ChatMembers WHERE memberId=${userId}`)]
        }
      },
      include: [
        {
          model: Model.ChatMember,
          attributes: ["id", "memberId", "unreadMessageCount", "status"],
          include: {
            model: Model.User,
            attributes: ["id", "fullName", "email"],
          }
        },
        { // to get chat channel in which a chat exist
          model: Model.Chat,
          required: true,
          attributes: [],
        }
      ],
      order: [
        ['updatedAt', 'DESC']
      ],
    })
  },
  updateUser: async (dataToUpdate, userId) => {
    return Model.User.update({ ...dataToUpdate }, { where: { id: userId } })
  },
  createChatChannel: async (senderId, receiverId, type, t) => {
    const chatChannel = await Model.ChatChannel.create({
      startFromUserId: senderId,
      type
    }, { transaction: t })
    const chatMembersData = [
      {
        channelId: chatChannel.id,
        memberId: senderId
      },
      {
        channelId: chatChannel.id,
        memberId: receiverId,
      }
    ]
    await Model.ChatMember.bulkCreate(chatMembersData, { transaction: t })
    return chatChannel
  },
  sendMessage: async function (senderId, receiverId, message, channelType, messageType, messageReplyId, io, socket) {
    const t = await sequelize.transaction()

    try {
      // const { senderId, receiverId, message, type, messageReplyId } = data
      let alreadyInChannel = await sequelize.query(`select * from ChatMembers where channelId in (select channelId from ChatMembers where ChatMembers.memberId = ${senderId}) and memberId = ${receiverId}`, { type: QueryTypes.SELECT })
      let channelId;
      let isFirstMessage = false // for keeping track if its first message then send newly created chathead to both message sender and receiver
      if (alreadyInChannel.length > 0) { // channel exist
        channelId = alreadyInChannel[0].channelId
      } else {  // channel does not exist create it
        const chatChannel = await this.createChatChannel(senderId, receiverId, channelType, t)
        channelId = chatChannel.id
        isFirstMessage = true
      }
      // getting the socketId of receiver user
      const receiver = await Model.User.findOne({ where: { id: receiverId } })
      const sender = await Model.User.findOne({ attributes: ["id", "fullName"], where: { id: senderId } })
      // store the message in chat table
      const chatRecord = await Model.Chat.create({ channelId, senderId, message, messageReplyId: messageReplyId || null, status: MESSAGE_STATUS.SENT, messageType: messageType || "text" }, { transaction: t })
      // setting unread message count
      await Model.ChatMember.increment('unreadMessageCount', { by: 1, where: { channelId, memberId: receiverId }, transaction: t })
      // update the last message in chat channel
      await Model.ChatChannel.update({ lastMessage: message, lastMessageType: messageType }, { where: { id: channelId }, transaction: t })
      await t.commit()
      // if receiver have socketId
      if (receiver.socketId) {
        if (isFirstMessage) {
          io.to(receiver.socketId).emit("chathead:get", await this.getChatChannels(receiverId))
          socket.emit("chathead:get", await this.getChatChannels(senderId))
        }
        // emit event to other receiver socket Id
        const returnData = JSON.parse(JSON.stringify(chatRecord))
        returnData['receiverId'] = receiverId;
        returnData['sender'] = sender;
        io.to(receiver.socketId).emit("message:receive", returnData);
        // sending push notification to the user
        const { unreadMessageCount } = await Model.ChatMember.findOne({ attributes: ['unreadMessageCount'], where: { channelId, memberId: receiverId } })
        notificationService.sendNotification(receiver.fcmToken, "Message:", message, JSON.stringify({ channelId, senderId, receiverId, unreadMessageCount, message }))
      }
      return { channelId, messageId: chatRecord.id }
    } catch (error) {
      console.log(error)
      await t.rollback()
    }
  },
  markMessagesAsRead: async (channelId, userId, otherUserId) => {
    await Promise.all([
      Model.ChatMember.update({ unreadMessageCount: 0 }, { where: { channelId, memberId: userId } }),
      Model.Chat.update({ status: MESSAGE_STATUS.READ }, { where: { senderId: otherUserId } })
    ])
  },
}

module.exports = helperFunctions